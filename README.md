## Named Entity Recognition In Slovene Language Using BERT

See our [paper](reports/final_report.pdf) for a description of our models and 
the process of fine-tuning them. All four models mentioned in the paper are
available in [models](models) folder.

Fine-tuning process can be performed by running `python training.py multilingual simple`,
where language can be either 'multilingual' or 'slovene' and type can either be
'simple' or 'full'. The specifics of these choices are described in the paper. Our
models were trained on an Nvidia GeForce GTX 1070 GPU with 8 GB of memory. For less
powerful GPUs, batch size should be adjusted.

Evaluation can be performed by running `python evaluation.py multilingual simple`.
Parameters are the same as in the fine-tuning process. The specified model will be 
loaded from the models folder and predictions for sentences in the test set will
be made and evaluated. 

Predictions for individual sentences can be made by creating an `EntityPredictor`
class like in the following example:
```python
from ssj500k_dataset import Ssj500kDataset
from prediction import EntityPredictor

dataset = Ssj500kDataset(tokenizer_language='multilingual', mode='test', all_labels=False)
predictor = EntityPredictor(entity_types=dataset.entity_types, model='multilingual-full')
print(predictor.predict_entities('Ameriški predsednik Donald Trump zaostruje svojo retoriko in odnose s Kitajsko.'))
```

The following dependencies were used for development:
* bs4 (0.0.1)
* keras (2.3.1)
* numpy (1.18.2)
* scikit-learn (0.22.2.post1)
* seqeval (0.0.12)
* torch (1.5.0+cu92)
* tqdm (4.45.0)
* transformers (2.9.1)
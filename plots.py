import matplotlib.pyplot as plt
import numpy as np


def add_y_grid(ax, max_y):
    ax.set_yticks(np.arange(0, max_y + 0.1, 0.1))
    ax.grid(b=True, axis='y')
    ax.set_axisbelow(True)


MODELS = ['Slovene [Full]', 'Multilingual [Full]', 'Slovene [Simple]', 'Multilingual[Simple]']
COLORS = ['#256EFF', '#46237A', '#3DDC97', '#FF495C']
TYPES = ['per', 'loc', 'org', 'misc', 'deriv-per']

# ========================================
#             TOTAL F-SCORE
# ========================================

scores = [0.8447, 0.8894, 0.8996, 0.9152]

_, ax = plt.subplots()
ax.bar(MODELS, scores, width=0.5, color=COLORS)
add_y_grid(ax, max_y=1)
ax.set_ylabel('F-score', fontsize=12)

# ========================================
#           F-SCORES BY TYPE
# ========================================

per_scores = [0.9857, 0.9731, 0.9667, 0.9667]
loc_scores = [0.9387, 0.9559, 0.9583, 0.9655]
org_scores = [0.8050, 0.9205, 0.7582, 0.9346]
misc_scores = [0.2944, 0.7649]
deriv_per_scores = [0.3333, 1.0000]

fig, ((ax11, ax12), (ax21, ax22), (ax31, ax32)) = plt.subplots(3, 2)
fig.set_size_inches(8, 10)

ax11.bar(MODELS, per_scores, width=0.5, color=COLORS)
ax11.set_xticklabels(MODELS, rotation=20, ha='right')
add_y_grid(ax11, max_y=1)
ax11.set_title(TYPES[0], fontsize=14)

ax12.bar(MODELS, loc_scores, width=0.5, color=COLORS)
ax12.set_xticklabels(MODELS, rotation=20, ha='right')
add_y_grid(ax12, max_y=1)
ax12.set_title(TYPES[1], fontsize=14)

ax21.bar(MODELS, org_scores, width=0.5, color=COLORS)
ax21.set_xticklabels(MODELS, rotation=20, ha='right')
add_y_grid(ax21, max_y=1)
ax21.set_title(TYPES[2], fontsize=14)

ax22.axis('off')

ax31.bar(MODELS[0:2], misc_scores, width=0.5, color=COLORS[2:4])
add_y_grid(ax31, max_y=1)
ax31.set_title(TYPES[3], fontsize=14)

ax32.bar(MODELS[0:2], deriv_per_scores, width=0.5, color=COLORS[2:4])
add_y_grid(ax32, max_y=1)
ax32.set_title(TYPES[4], fontsize=14)

# =====================================================
#   REAL AND PREDICTED TYPE PROBABILITY DISTRIBUTIONS
# =====================================================

real_full = [0.4077, 0.1846, 0.2338, 0.1616, 0.0123]
predicted_slovene_full = [0.5373, 0.2151, 0.2076, 0.0368, 0.0032]
predicted_multilingual_full = [0.4456, 0.1949, 0.1154, 0.2299, 0.0142]

real_simple = [0.5010, 0.2202, 0.2789]
predicted_slovene_simple = [0.5570, 0.2407, 0.2023]
predicted_multilingual_simple = [0.5101, 0.2236, 0.2662]

bar_width = 0.2
bar_gap = 0.05
fig, (ax1, ax2) = plt.subplots(2, 1)
fig.set_size_inches(6, 5)

x_ticks = np.arange(0, len(real_full), 1)
ax1.bar(x_ticks - (bar_width + bar_gap), real_full, bar_width, label='Real', color='#FF8C42')
ax1.bar(x_ticks, predicted_slovene_full, bar_width, label='Slovene [Full]', color=COLORS[0])
ax1.bar(x_ticks + (bar_width + bar_gap), predicted_multilingual_full, bar_width, label='Multilingual [Full]', color=COLORS[1])
ax1.set_xticks(x_ticks)
ax1.set_xticklabels(TYPES)
ax1.legend()
add_y_grid(ax1, max_y=0.5)

x_ticks = np.arange(0, len(real_simple), 1)
ax2.bar(x_ticks - (bar_width + bar_gap), real_simple, bar_width, label='Real', color='#FF8C42')
ax2.bar(x_ticks, predicted_slovene_simple, bar_width, label='Slovene [Simple]', color=COLORS[2])
ax2.bar(x_ticks + (bar_width + bar_gap), predicted_multilingual_simple, bar_width, label='Multilingual [Simple]', color=COLORS[3])
ax2.set_xticks(x_ticks)
ax2.set_xticklabels(TYPES[:3])
ax2.legend()
add_y_grid(ax2, max_y=0.7)

plt.show()

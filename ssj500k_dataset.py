from collections import defaultdict

from bs4 import BeautifulSoup
from transformers import BertTokenizer


class Ssj500kDataset():
    DATA_PATH = 'data/ssj500k/ssj500k.xml'
    SLOVENE_TOKENIZER_PATH = 'models/slovene-pretrained'
    MULTILINGUAL_TOKENIZER_NAME = 'bert-base-multilingual-cased'
    PADDING_LABEL = 'pad'
    OTHER_LABEL = 'othr'
    MISC_LABEL = 'misc'
    DERIV_PER_LABEL = 'deriv-per'
    PER_LABEL = 'per'
    ORG_LABEL = 'org'
    LOC_LABEL = 'loc'

    def __init__(self, tokenizer_language, mode='all', all_labels=True):
        self.sentences = []
        self.labels = []

        # define entity types:
        if all_labels:
            self.entity_types = [self.PADDING_LABEL, self.OTHER_LABEL, self.PER_LABEL, self.ORG_LABEL, self.LOC_LABEL,
                                 self.MISC_LABEL, self.DERIV_PER_LABEL]
        else:
            self.entity_types = [self.PADDING_LABEL, self.OTHER_LABEL, self.PER_LABEL, self.ORG_LABEL, self.LOC_LABEL]

        # map entity types to indices
        self.type_indices = {t: i for i, t in enumerate(self.entity_types)}

        # instantiate tokenizer
        if tokenizer_language == 'slovene':
            tokenizer = BertTokenizer.from_pretrained(self.SLOVENE_TOKENIZER_PATH, do_lower_case=False).basic_tokenizer
        elif tokenizer_language == 'multilingual':
            tokenizer = BertTokenizer.from_pretrained(self.MULTILINGUAL_TOKENIZER_NAME, do_lower_case=False).basic_tokenizer
        else:
            raise ValueError('Specified language is not supported')

        # read corpus file
        with open(self.DATA_PATH, 'r', encoding='utf-8') as f:
            self.tei = BeautifulSoup(f, 'lxml')

        # decide which part of the corpus to use depending on mode
        data = self.tei.findAll('s')
        if mode == 'train':
            corpus = [s for i, s in enumerate(data) if i % 10 != 0]
        elif mode == 'test':
            corpus = [data[i] for i in range(0, len(data), 10)]
        else:
            corpus = data

        # parse words and their labels from corpus
        for s in corpus:
            self.sentences.append([])
            self.labels.append([])
            for tag in s.findAll(['w', 'pc', 'seg'], recursive=False):
                if tag.name == 'w' or tag.name == 'pc':
                    tokens = tokenizer.tokenize(tag.text)
                    self.sentences[-1].extend(tokens)
                    self.labels[-1].extend([self.OTHER_LABEL] * len(tokens))
                elif tag.name == 'seg':
                    # get entity's label
                    label = tag.get('subtype')
                    if not all_labels:
                        # modify misc or deriv-per labels into more common ones
                        if label == self.MISC_LABEL:
                            label = self.OTHER_LABEL
                        elif label == self.DERIV_PER_LABEL:
                            label = self.PER_LABEL

                    # tokenize entity and its tokens and labels to sentences and labels lists
                    entity = tokenizer.tokenize(tag.text)
                    self.sentences[-1].extend(entity)
                    self.labels[-1].extend([label] * len(entity))

    def print_statistics(self):
        type_counts = defaultdict(int)
        for l in self.labels:
            for type in l:
                type_counts[type] += 1

        print("No. of words: {}".format(sum(type_counts.values())))
        print("Type counts:")
        for t, c in sorted(type_counts.items(), key=lambda item: item[1], reverse=True):
            print("\t{}: {}".format(t, c))

import sys
from collections import defaultdict

from tqdm.contrib import tzip

from prediction import EntityPredictor
from ssj500k_dataset import Ssj500kDataset


def calculate_metrics(cm):
    """Calculates precision, recall and F1 score from a given confusion matrix in form [tp, tn, fp, fn]"""
    precision = cm[0] / (cm[0] + cm[2]) if cm[0] + cm[2] != 0 else 1
    recall = cm[0] / (cm[0] + cm[3]) if cm[0] + cm[3] != 0 else 1
    return precision, recall, 2 * (precision * recall) / (precision + recall)


# parse language and type of model to train from arguments
if len(sys.argv) == 3:
    language = sys.argv[1]
    typ = sys.argv[2]
    if language not in ['slovene', 'multilingual'] or typ not in ['simple', 'full']:
        raise ValueError("Given language or type is not valid")
    full = 'full' == typ
else:
    raise ValueError("Wrong number of arguments given")

dataset = Ssj500kDataset(language, mode='test', all_labels=full)
predictor = EntityPredictor(dataset.entity_types, model='{}-{}'.format(language, typ))
confusion_matrices = defaultdict(lambda: [0, 0, 0, 0])
for sentence, true_labels in tzip(dataset.sentences, dataset.labels):
    # predict labels
    predicted_labels = predictor.predict_entities(' '.join(sentence))

    for true, predicted in zip(true_labels, predicted_labels):
        # count true positives and negatives and false positives and negatives and add them to confusion matrices
        if true == predicted and true != dataset.OTHER_LABEL:
            confusion_matrices['all'][0] += 1
            confusion_matrices[true][0] += 1
        elif true == predicted and true == dataset.OTHER_LABEL:
            confusion_matrices['all'][1] += 1
            confusion_matrices[true][1] += 1
        elif true != predicted and true == dataset.OTHER_LABEL:
            confusion_matrices['all'][2] += 1
            confusion_matrices[true][2] += 1
        elif true != predicted and true != dataset.OTHER_LABEL:
            confusion_matrices['all'][3] += 1
            confusion_matrices[true][3] += 1

print("Total accuracy: {:.4f}".format((confusion_matrices['all'][0] + confusion_matrices['all'][1])
                                      / sum(confusion_matrices['all'])))
precision, recall, f1 = calculate_metrics(confusion_matrices['all'])
print("Total F-score: {:.4f}".format(f1))
print("Total precision: {:.4f}".format(precision))
print("Total recall: {:.4f}".format(recall))

print("F-score by entity type:")
for t, cm in confusion_matrices.items():
    if t != 'all' and t != 'othr':
        _, _, f1 = calculate_metrics(cm)
        print("\t{}: {:.4f}".format(t, f1))

print("No. of predictions by entity type:")
for t, cm in confusion_matrices.items():
    if t != 'all' and t != 'othr':
        print("\t{}: {}".format(t, cm[0] + cm[2]))

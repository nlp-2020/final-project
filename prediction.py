import numpy as np
import torch
from transformers import BertForTokenClassification, BertTokenizer


class EntityPredictor():

    def __init__(self, entity_types, model='slovene-full'):
        if model == 'slovene-full':
            self.model = BertForTokenClassification.from_pretrained('models/slovene-full')
            self.tokenizer = BertTokenizer.from_pretrained('models/slovene-pretrained', do_lower_case=False)
        elif model == 'slovene-simple':
            self.model = BertForTokenClassification.from_pretrained('models/slovene-simple')
            self.tokenizer = BertTokenizer.from_pretrained('models/slovene-pretrained', do_lower_case=False)
        elif model == 'multilingual-full':
            self.model = BertForTokenClassification.from_pretrained('models/multilingual-full')
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
        elif model == 'multilingual-simple':
            self.model = BertForTokenClassification.from_pretrained('models/multilingual-simple')
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
        else:
            raise ValueError('Specified model is not valid')
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model.to(self.device)
        self.entity_types = entity_types

    def predict_entities(self, sentence):
        """Predicts entities in a string"""
        # tokenize sentence
        tokenized_sentence = self.tokenizer.encode(sentence)
        sentence_embedding = torch.tensor([tokenized_sentence]).to(self.device)

        # make predictions
        with torch.no_grad():
            output = self.model(sentence_embedding)
        label_indices = np.argmax(output[0].cpu().numpy(), axis=2)

        # convert predicted entity indices to actual labels
        tokens = self.tokenizer.convert_ids_to_tokens(sentence_embedding.cpu().numpy()[0])
        new_tokens, new_labels = [], []
        for token, label_index in zip(tokens, label_indices[0]):
            if not token.startswith('##'):
                new_labels.append(self.entity_types[label_index])

        # omit start and end of sentence tags
        return new_labels[1:-1]

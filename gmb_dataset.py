import pandas as pd


class GMBDataset:
    PATH = 'data/groningen_meaning_bank/gmb_dataset.csv'

    def __init__(self, mode):
        self.sentences = []
        self.labels = []

        # read data
        self.data = pd.read_csv(self.PATH, encoding='latin1').fillna(method='ffill')

        # restructure data into individual sentences and parse them
        if mode == 'train':
            self.grouped = list(self.data.groupby('Sentence #'))[:-500]
        elif mode == 'test':
            self.grouped = list(self.data.groupby('Sentence #'))[-500:]
        for sentence in self.grouped:
            self.sentences.append(list(sentence[1]['Word']))
            self.labels.append(list(sentence[1]['Tag']))

        # extract all possible classes (entity types)
        self.entity_types = list(self.data['Tag'].unique())
        # append class which will be used for embedding padding
        self.entity_types.append('PAD')
        self.type_indices = {t: i for i, t in enumerate(self.entity_types)}

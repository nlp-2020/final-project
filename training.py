import sys

import numpy as np
import torch
from keras_preprocessing.sequence import pad_sequences
from seqeval.metrics import f1_score
from sklearn.model_selection import train_test_split
from torch.utils.data import TensorDataset, RandomSampler, DataLoader, SequentialSampler
from tqdm import trange
from transformers import BertTokenizer, BertForTokenClassification, AdamW, get_linear_schedule_with_warmup

from ssj500k_dataset import Ssj500kDataset

MAX_EMBEDDING_SIZE = 128
BATCH_SIZE = 32
VALIDATION_SET_FRACTION = 0.1
# determine whether to update all weights or just the ones of the linear classifier on top of BERT
FULL_FINETUNING = True
EPOCHS = 3
MAX_GRAD_NORM = 1.0


# calculate accuracy of model predictions
def calculate_accuracy(preds, labels):
    return np.sum(np.argmax(preds, axis=2) == labels) / np.size(labels)


# parse language and type of model to train from arguments
if len(sys.argv) == 3:
    language = sys.argv[1]
    typ = sys.argv[2]
    if language not in ['slovene', 'multilingual'] or typ not in ['simple', 'full']:
        raise ValueError("Given language or type is not valid")
    pretrained = 'models/slovene-pretrained' if language == 'slovene' else 'bert-base-multilingual-cased'
    full = 'full' == typ
else:
    raise ValueError("Wrong number of arguments given")

dataset = Ssj500kDataset(tokenizer_language=language, mode='train', all_labels=full)

# setup torch device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# tokenize sentences and transfer all word labels to their respective tokens
tokenizer = BertTokenizer.from_pretrained(pretrained, do_lower_case=False)
tokenized_sentences = []
tokenized_labels = []
for sentence, labels in zip(dataset.sentences, dataset.labels):
    tokenized_sentence = []
    token_labels = []
    for word, label in zip(sentence, labels):
        tokenized_word = tokenizer.tokenize(word)
        tokenized_sentence.extend(tokenized_word)
        token_labels.extend([label] * len(tokenized_word))
    tokenized_sentences.append(tokenized_sentence)
    tokenized_labels.append(token_labels)

# cut or pad tokenized sentences and labels
sentence_embeddings = pad_sequences([tokenizer.convert_tokens_to_ids(t) for t in tokenized_sentences],
                                    maxlen=MAX_EMBEDDING_SIZE, dtype='int64', value=0.0, padding='post',
                                    truncating='post')
entity_type_embeddings = pad_sequences([[dataset.type_indices.get(l) for l in lab] for lab in tokenized_labels],
                                       maxlen=MAX_EMBEDDING_SIZE, dtype='int64',
                                       value=dataset.type_indices[dataset.PADDING_LABEL],
                                       padding='post', truncating='post')

# create attention masks to ignore embedding paddings
attention_masks = [[float(token != 0.0) for token in sentence] for sentence in sentence_embeddings]

# split dataset into train and validation sets
seed = 2020
train_sentences, validation_sentences, train_labels, validation_labels = train_test_split(sentence_embeddings,
                                                                                          entity_type_embeddings,
                                                                                          random_state=seed,
                                                                                          test_size=VALIDATION_SET_FRACTION)
train_masks, validation_masks, _, _ = train_test_split(attention_masks, sentence_embeddings, random_state=seed,
                                                       test_size=VALIDATION_SET_FRACTION)

# create PyTorch tensors
train_sentences = torch.tensor(train_sentences)
validation_sentences = torch.tensor(validation_sentences)
train_labels = torch.tensor(train_labels)
validation_labels = torch.tensor(validation_labels)
train_masks = torch.tensor(train_masks)
validation_masks = torch.tensor(validation_masks)

# define dataloaders
train_data = TensorDataset(train_sentences, train_masks, train_labels)
# shuffle training data using RandomSampler
train_sampler = RandomSampler(train_data)
train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=BATCH_SIZE)

validation_data = TensorDataset(validation_sentences, validation_masks, validation_labels)
# pass validation data sequentially using SequentialSampler
validation_sampler = SequentialSampler(validation_data)
validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=BATCH_SIZE)

# create BertForTokenClassification model which is a BertModel with added token-level classifier on top
model = BertForTokenClassification.from_pretrained(pretrained, num_labels=len(dataset.type_indices),
                                                   output_attentions=False, output_hidden_states=False)
model.cuda()

# create optimizer specify which parameters it will optimize
if FULL_FINETUNING:
    # optimize all parameters if full fine-tuning is requested
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.0}]
else:
    # only optimize parameters of classifier on top of BERT if full fine-tuning is not requested
    param_optimizer = list(model.classifier.named_parameters())
    optimizer_grouped_parameters = [{'params': [p for n, p in param_optimizer]}]
optimizer = AdamW(optimizer_grouped_parameters, lr=3e-5, eps=1e-8)

# create scheduler to linearly reduce learning rate between epochs
total_steps = len(train_dataloader) * EPOCHS
scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)

# keep track of training and validation loss values so we can plot them
training_loss_values, validation_loss_values = [], []

# fine-tune model
for i in trange(EPOCHS, desc='Epoch'):
    # ========================================
    #               training
    # ========================================
    # perform one full pass over the training set

    # put model in training mode
    model.train()
    # reset total loss for this epoch
    total_loss = 0

    # training loop
    for step, batch in enumerate(train_dataloader):
        # add batch to GPU
        batch = tuple(t.to(device) for t in batch)
        b_sentence_embeddings, b_attention_masks, b_labels = batch
        # clear any previously calculated gradients before performing a backward pass
        model.zero_grad()
        # forward pass
        outputs = model(b_sentence_embeddings, attention_mask=b_attention_masks, labels=b_labels, token_type_ids=None)
        # get loss
        loss = outputs[0]
        # perform backward pass to calculate gradients
        loss.backward()
        # track training loss
        total_loss += loss.item()
        # clip the norm of gradient to help prevent exploding gradients
        torch.nn.utils.clip_grad_norm_(parameters=model.parameters(), max_norm=MAX_GRAD_NORM)
        # update parameters
        optimizer.step()
        # update learning rate
        scheduler.step()

    # calculate average loss over training data
    avg_train_loss = total_loss / len(train_dataloader)
    print("Average training loss: {}".format(avg_train_loss))
    training_loss_values.append(avg_train_loss)

    # ========================================
    #               validation
    # ========================================
    # measure performance on validation set after completion of each training epoch

    # put model in evaluation mode
    model.eval()
    # reset validation loss for this epoch
    eval_loss = 0
    num_eval_steps, num_eval_examples = 0, 0
    predictions, true_labels = [], []
    for batch in validation_dataloader:
        batch = tuple(t.to(device) for t in batch)
        b_sentence_embeddings, b_attention_masks, b_labels = batch

        # don't compute or store gradients to save memory and speed up validation
        with torch.no_grad():
            # forward pass, calculate logit predictions
            outputs = model(b_sentence_embeddings, attention_mask=b_attention_masks, labels=b_labels,
                            token_type_ids=None)
        # move logits and labels to CPU
        logits = outputs[1].detach().cpu().numpy()
        label_ids = b_labels.cpu().numpy()

        # calculate accuracy for this batch of validation sentences
        eval_loss += outputs[0].mean().item()
        predictions.extend([list(p) for p in np.argmax(logits, axis=2)])
        true_labels.extend(label_ids)

        num_eval_examples += b_sentence_embeddings.size(0)
        num_eval_steps += 1

    eval_loss /= num_eval_steps
    validation_loss_values.append(eval_loss)
    print("Validation loss: {}".format(eval_loss))
    predicted_labels = [dataset.entity_types[p_i] for p, l in zip(predictions, true_labels) for p_i, l_i in zip(p, l)
                        if dataset.entity_types[l_i] != dataset.PADDING_LABEL]
    valid_labels = [dataset.entity_types[l_i] for l in true_labels for l_i in l if
                    dataset.entity_types[l_i] != dataset.PADDING_LABEL]
    print("Validation accuracy: {}".format(
        sum([x == y for (x, y) in zip(predicted_labels, valid_labels)]) / len(valid_labels)))
    print('Validation F1-score: {}'.format(f1_score(valid_labels, predicted_labels)))
    print()

# plot training and validation loss values
# fig, ax = plt.subplots()
# ax.plot(training_loss_values, 'b-', label='training loss')
# ax.plot(validation_loss_values, 'r-', label='validation loss')
# ax.legend()
# ax.set_title('Loss progression during training', fontsize=14)
# ax.set_xlabel('Epoch', fontsize=12)
# ax.set_ylabel('Loss', fontsize=12)
# plt.show()

# save trained model
model.save_pretrained('models/{}-{}'.format(language, typ))
